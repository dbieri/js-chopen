## Übung 1: Von einer naiven JavaScript Applikation zu einem modernen Front-End Build:

### Teil 1: Refactoring der naiven Applikation

- Wechseln Sie in das Verzeichnis `01-Intro/01-SimplisticToDoNaive`
- Öffnen Sie `index.html` in einem Browser und fügen Sie neue ToDo-Items hinzu
- Inspizieren Sie die Applikation und den zugehörigen JavaScript Code
- Versuchen Sie den JavaScript Code zu debuggen
- Refactoren Sie die Applikation:
	- Trennen Sie den JavaScript Code vom HTML Code.
	- Schreiben Sie unobtrusive JavaScriptCode

### Teil 2: Moderner Front-End Build
Vorbereitung: Installieren Sie Node, NPM und die nötigen Node Packages gemäss der Anleitung in `PREPARATION.md`

- Wechseln Sie in das Verzeichnis `01-Intro/02-SimplisticToDo-Gulp`
- Öffnen Sie `src/index.html` in einem Browser
- Führen Sie die verschiedenen Schritte des Frontend-Builds aus wie sie im README beschrieben sind.
- Inspizieren Sie das Ergebnis des Builds im Directory `dist`. Öffnen sie `dist/index.html`, was ist der Unterschied zu `src/index.html`
- Advanced: Ändern die das Build-Script so, dass die Karma-Tests bei einem vollständigen Build immer ausgeführt werden.


<br/>
## Übung 2: Sprach-Konstrukte

In diesem Block lernen Sie verschiedene Themen im Zusammenhang mit der Programmiersprache JavaScript kennen. Dazu arbeiten Sie “automatisierten Lektionen” in Form von BDD-Tests durch. Wählen Sie die Themen welche sie am meisten interessieren. Versuchen Sie nicht alle Themen zu lösen, dazu wird die Zeit nicht reichen.

- Wechseln Sie in das Verzeichnis `02-Language/99-LanguageLessons`
- Öffnen Sie `index.html` im Browser und navigieren Sie zu dem Thema das Sie bearbeiten möchten. Sie sehen einen fehlschlagenden Test.
- Öffnen Sie das zugehörige JavaScript File. z.B. `01-Basics/AboutTheBasics.js` und editieren sie den Code so dass der Test nicht mehr fehlschlägt.
- Führen sie einen Refresh im Browser durch. Der nächste Test schlägt fehl...



## Übung 3: Capturing A Context with A Closure
Untersuchen Sie das Beispiel `03-Patterns/05-CapturingScope/index.html`. Klicken Sie auf die Zahlen: Verhält sich das Beispiel wie erwartet?  
Ändern Sie das Beispiel so, dass bei einem Klick auch die entsprechende Zahl im Alert erscheint. Verwenden Sie dazu Closures.



## Übung 4: Von Spaghetti zu Modulen
Untersuchen Sie das Beispiel in `03-Patterns/03-Spaghetti`: Erweitern Sie das File `index.html` indem sie das zweite Modul einbinden (auskommentierter Code). Welchen Seiteneffekt beobachten Sie vom zweiten Modul auf das erste Modul?

Bauen Sie das Beispiel in  so um, dass sich die beiden Module nicht mehr beeinflussen:

- Die Settings von Modul 1 sollen über das UI gespeichert und geladen werden können
- Die Settings von Modul 2 sollen beim Laden der Seite gespeichert und im Log ausgegeben werden.

Verwenden Sie Closures/IIFEs um die Module zu isolieren.


<br/>
<br/>
## Übung 5: Calculator
Studieren Sie das Beispiel des Revealing Module Pattern: `03-Patterns/05-RevealingModule/RevealingModuleDemos/calculator`

- Implementieren Sie die fehlende Funktionalität des Buttons '←'
 


## Übung 6: Promises
Wechseln Sie in das Verzeichnis `03-Patterns/20-Promises/90-Exercise`.

Starten Sie den Node Server:

```
cd server
npm install
node server.js
```

Sie können nun über die URL `http://localhost:3456/word/0` ein Wort abfragen. Der letze Parameter der URL kann zwischen 0-8 variiert werden.

Öffnen Sie die Website `app/index.html`: Die Logik lädt das erste Wort vom Server und rendert es auf der Seite.
Erweitern Sie die Logik mit zwei Varianten:

- Alle Wörter sollen gleichzeitig abgefragt werden. Die Wörter sollen aber erst auf der Seite dargestellt werden, wenn Sie alle geladen wurden.
- Die Wörter sollen eines nach dem anderen vom Server geladen werden und jeweils auf der Seite gerendert werden.

Verwenden Sie Promises um diese Logik zu implementieren.


## 7. NPM & Bower

Analog der Demo: Erstellen sie ein neues Projekt mit npm und Bower. Verwenden Sie npm um Grunt und JSHint als Build-Dependencies zu integrieren und Bower um jQuery als Web-Asset zu integrieren.

Führen Sie dazu die folgenden Schritte aus und versuchen Sie diese zu verstehen:

- `npm install -g bower grunt-cli` oder `npm install -g bower gulp`
- `npm init`
- `npm install grunt grunt-contrib-jshint --save-dev` oder `npm install gulp gulp-jshint --save-dev`
- `bower init`
- `bower install jquery --save`
- Erstellen Sie ein `index.html` und ein `app.js`. Binden Sie `app.js` in die html-Seite ein und manipulieren Sie das DOM z.B. `$('body').html('Hello from jQuery)'`
- Erstellen sie ein `gruntfile.js` oder ein `gulpfile.js` welches auf den JavaScript sources des Projektes ein Linting durchführt (siehe dazu auch die Slides).
- Führen Sie den Build aus mit `grunt`.
- (Fakultativ) Binden sie nun auch JSCS als zusätzlichen statischen Code-Analyzer ein.
- Binden sie ein zweites JavaScript file ein (`module.js`), das z.B. ein weiteres Element an dem Body anhängt. Erweitern Sie den Build, so dass `app.js` und `module.js` konkateniert und minifiziert werden. Benutzen Sie z.B. uglify.js resp. ein passendes Grunt/Gulp Plugin dazu.



## 8. Yeoman

Verwenden Sie Yeoman um ein Angular Applikation zu generieren. 

Führen Sie dazu die folgenden Schritte aus und versuchen Sie diese zu verstehen:

```
npm install -g yo
npm install -g generator-angular
yo angular
grunt
grunt serve
grunt watch
```


## 9. Gulp vs. Grunt

Vergleichen Sie das Beispiel in `01-Intro/02-SimpisticToDo-Gulp` mit dem Demo Beispiel `01-Intro/02-SimpisticToDo-Grunt`.

Installieren Sie die nötigen Dependencies mit `npm install` und `bower install`.

Führen Sie den Build aus mit `grunt` resp `gulp`. Untersuchen Sie das Ergebnis.

Verändern sie den Build, so dass die Karma-Tests jeweils als Teil des default builds ausgeführt werden.

Untersuchen Sie die beiden Build files `gruntfile.js` resp. `gulpfile.js`. Welches ist für Sie verständlicher? Welche Vorteile/Nachteile sehen Sie?



## 10. Testing mit Karma

Implementieren Sie die klassische 'Bowling Kata' (http://butunclebob.com/ArticleS.UncleBob.TheBowlingGameKata) in JavaScript.

- Starten Sie, indem sie ein Karma-Projekt initialisieren:

		npm install -g karma
		npm install karma
		karma init
			testing framework: jasmine
			Require.js: no
			browser: Chrome
			files: src/**/*.js
			excluded: <empty>
			watch: yes
		karma start
	
- erstellen Sie das File `src/bowlingGameSpec.js` und schreiben sie den ersten Test:

		describe('Bowling Game', function(){
    		it('should calculate the score for a gutter game', function(){
        		var game = new BowlingGame();
        		for (var i = 0; i < 20; i++){
            		game.roll(0);
        		}
        		expect(game.score()).toEqual(0);
    		})
		});
		
- erstellen Sie das File `src/bowlingGame.js` und beginnen Sie mit der Implementation:

		var BowlingGame = function(){
			this.rolls = [];
			this.rollCount = 0;
			
			this.roll = function(pins){
				this.rolls[this.rollCount] = pins;
				this.rollCount += 1;
			}
			
			this.getScore = function(){
				var score = 0;
				this.rolls.forEach(function(pins){
					score += pins;
				})
				return score;
			}
		}

- erstellen Sie ein weiterer Tests nach dem anderen und erweitern Sie jeweils die Implementation:
    	
		it("should calculate the score for all ones", function() {
        	rollMany(20, 1);
       	 	expect(game.getScore()).toEqual(20);
    	});
    	
		it("should calculate the score for one spare", function() {
        	rollSpare();
        	game.roll(3);
        	rollMany(17, 0);
        	expect(game.score()).toEqual(16);
    	});
    	
	    it("should calculate the score for one strike", function() {
        	rollStrike();
        	game.roll(3);
        	game.roll(4);
        	rollMany(16, 0);
        	expect(game.getScore()).toEqual(24);
    	});
    	
    	it("should calculate the score for a perfect game", function() {
        	rollMany(12, 10);
        	expect(game.getScore()).toEqual(300);
    	});

<br/>
## Übung 11: Plain MVC

#### 1. Simplest Example
Erweitern Sie das Beispiel `03-Patterns/30-MVC/01-Simplest` so, dass unterhalb des Logos auch ein URL-Link auf die Homepage des Frameworks angezeigt wird.

#### 2. ToDo List
Schreiben Sie die ToDo Liste aus dem Beispiel `03-Patterns/30-MVC/02-SimplisticToDo-NoMVC` so um, dass das MVC Pattern verwendet wird. Der State sollte dabei aus dem DOM in ein JavaScript Objekt Model extrahiert werden. 
