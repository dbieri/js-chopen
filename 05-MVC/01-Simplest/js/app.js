/* ======= Model ======= */

var model = {
    currentFramework: null,
    frameworks: [
        {
            likeCount: 0,
            name: 'Backbone',
            imgSrc: 'img/backbone.jpg',
            url: 'http://backbonejs.org/'
        },
        {
            likeCount: 0,
            name: 'Marionette',
            imgSrc: 'img/marionette.png',
            url: 'http://marionette.com/'
        },
        {
            likeCount: 0,
            name: 'EmberJS',
            imgSrc: 'img/ember.jpg',
            url: 'http://emberjs.com/'
        },
        {
            likeCount: 0,
            name: 'Knockout',
            imgSrc: 'img/knockout.jpg',
            url: 'http://knockoutjs.com/'
        },
        {
            likeCount: 0,
            name: 'AngularJS',
            imgSrc: 'img/angular.jpg',
            url: 'https://angularjs.org/'
        },
        {
            likeCount: 0,
            name: 'React',
            imgSrc: 'img/react.png',
            url: 'http://facebook.github.io/react/'
        }

    ]
};


/* ======= Controller ======= */

var controller = {

    init: function () {
        // set initial framework
        model.currentFramework = model.frameworks[0];

        frameworkListView.init();
        frameworkView.init();

        // use the event infrastructure of jQuery
        $(frameworkListView).on('item-selected', function (event, framework) {
            setCurrentFramework(framework)
        });

        $(frameworkView).on('like', incrementLikes);

        frameworkListView.render(model.frameworks);
        frameworkView.render(model.currentFramework);

        function setCurrentFramework(framework) {
            // update state in the model
            model.currentFramework = framework;
            frameworkView.render(model.currentFramework);
        }

        function incrementLikes() {
            // increments the likes for the currently-selected framework
            model.currentFramework.likeCount++;
            frameworkView.render(model.currentFramework);
        }
    }
};


/* ======= View ======= */

var frameworkView = {

    init: function () {
        this.frameworkNameElem = document.getElementById('framework-name');
        this.logoImageElem = document.getElementById('logo-img');
        this.frameworkUrl = document.getElementById('framework-url');
        this.likeCountElem = document.getElementById('like-count');
        this.likeButtonElem = document.getElementById('like-button');

        var $frameworkView = $(this); // use the event infrastructure of jQuery
        this.likeButtonElem.addEventListener('click', function () {
            $frameworkView.trigger('like');
        });
    },

    render: function (currentFramework) {
        this.likeCountElem.textContent = currentFramework.likeCount;
        this.frameworkNameElem.textContent = currentFramework.name;
        this.logoImageElem.src = currentFramework.imgSrc;
        this.frameworkUrl.textContent = currentFramework.url;
    }
};

var frameworkListView = {

    init: function () {
        this.frameworkListElem = document.getElementById('framework-list');
    },

    render: function (frameworks) {
        var framework, elem, i;
        var $frameworkListView = $(this); // use the event infrastructure of jQuery

        // Clear the list
        this.frameworkListElem.innerHTML = '';

        for (i = 0; i < frameworks.length; i++) {
            framework = frameworks[i];

            elem = document.createElement('li');
            elem.textContent = framework.name;

            elem.addEventListener('click', (function (framework) {
                // Capture context in a closure
                return function () {
                    $frameworkListView.trigger('item-selected', framework);
                };
            })(framework));

            this.frameworkListElem.appendChild(elem);
        }
    }
};

// make it go!
controller.init();