var calc;

module('Calculator', {
    setup: function() {

        var f = jasmine.getFixtures();
        f.fixturesPath = 'base/app';
        f.load('test/TestFixture.html');

        calc = new Calculator($("#div1"));
    },
    teardown: function() {
    }
});

test('add method', function() {
    strictEqual(calc.add(1, 1), 2);
});

test('divide method', function() {
    strictEqual(calc.divide(4, 2), 2);
});

test('DOM interaction when adding', function() {
    strictEqual(calc.add(1, 2), 3);
    strictEqual($(".calc-result").length, 1);
    strictEqual($(".calc-result").text(), "3");
});
