var Calculator = function(el) {
    this.resultElement = el;
};

Calculator.prototype.add = function(a, b) {
    var res = a + b;
    $(this.resultElement).append("<div class='calc-result'>" + res+ "</div>");
    return res;
};

Calculator.prototype.divide = function(a, b) {
    var res = a / b;
    $(this.resultElement).append("<div class='calc-result'>" + res+ "</div>");
    return res;
};
