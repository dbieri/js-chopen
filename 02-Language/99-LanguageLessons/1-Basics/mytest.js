/**
 * Created by dbieri on 01.09.2015.
 */

var arr = [1, 2, 3];
var out = [];

for (var i = 0; i < arr.length; i++) {
    var item = arr[i]; // there is only one 'item' variable that lives on the scope of the outer function
    out.push(function () {
        return item;
    });
}

var result = "";
out.forEach(function (func) {
    result += func();
});

console.log(result);