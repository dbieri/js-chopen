function Person(first, last) {
    this.first = first;
    this.last = last;

    this.fullName = function() {
    	return this.first + ' ' + this.last;
	};
}

var pers = new Person("John", "Doe");
console.log(pers.fullName());

var pers2 = new Person("Jane", "Jolly");
console.log(pers2.fullName());