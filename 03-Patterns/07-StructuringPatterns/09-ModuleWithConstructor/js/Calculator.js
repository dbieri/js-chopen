
var Calculator = function () {
    // private members - typically imported from another module
    var internalAdd = function(x,y) {
        return x + y;
    }

    Constr = function(eq) {
        this.eqCtl = document.getElementById(eq);
    }

    Constr.prototype = {
        add: function (x, y) {
            var val = internalAdd(x,y);
            this.eqCtl.innerHTML = val;
        }
    }

    return Constr;
}();

