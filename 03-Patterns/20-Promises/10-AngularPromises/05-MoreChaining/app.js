var app = angular.module('app', []);

app.controller('MainController', function ($q) {
    var vm = this;

    vm.mydata = "Initial data... ";

    var getUser = function(){

        return $q(function(resolve, reject){
            setTimeout(function(){resolve("User ... ")}, 3000);
        });
    };

    var getUserData = function(val){
        vm.mydata += val;

        return $q(function(resolve, reject){
            setTimeout(function(){resolve("User Data ... ")}, 3000);
        });
    };

    var updateUserData = function(val){
        vm.mydata += val;

        return $q(function(resolve, reject){
            setTimeout(function(){resolve("Updated Data ... ")}, 3000);
        });
    };

    var requestFinished = function(val){
        vm.mydata += "Request Finished";
    };

    var errorHandler = function(){
        alert("Something went wrong!");
    };

    getUser()
        .then(getUserData)
        .then(updateUserData)
        .catch(errorHandler)
        .finally(requestFinished);

});
