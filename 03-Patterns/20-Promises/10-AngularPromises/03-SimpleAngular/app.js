var app = angular.module('app', []);

app.controller('MainController', function($q) {
  var vm = this;

  vm.mydata = "Initial data... ";

  var promise = $q(function(resolve, reject){
    setTimeout(function(){
      resolve("More Data");
    }, 3000)
  });

  promise
    .then(function(val) {
      vm.mydata += val
      return val
    });
    // You can chain on more handlers for the promise.
    //.then(function(val) {
    //  vm.mydata += val
    //  return val
    //})
    //.then(function(val) {
    //  vm.mydata += val
    //  return val
    //})
});
