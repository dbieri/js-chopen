var app = angular.module('app', []);

app.controller('MainController', function ($q) {

    var vm = this;

    vm.mydata = "Initial data... ";


    var promise = $q(function (resolve, reject) {
        setTimeout(function () {
            resolve("More data ... ");
        }, 3000);
    });

    promise
        .then(function (val) {
            vm.mydata += val;

            // return another promise
            return $q(function (resolve, reject) {
                setTimeout(function () {
                    resolve("Even more data ... ")
                }, 3000)
            });
        })
        .then(function (val) {
            // This is only executed when the second promise is resolved
            vm.mydata += val;
        });


});
