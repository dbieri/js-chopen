### VmWare Image

Image downloaded from:
[http://www.osboxes.org/xubuntu/]

User: osboxes  
Password: osboxes.org


#### Installed Software
- VMWare Tools installed
- Chrome (deb downloaded)
- Web Storm (downloaded)
- Firefox Developer Edition (downloaded)

```
sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo add-apt-repository ppa:webupd8team/atom
sudo add-apt-repository ppa:webupd8team/brackets

sudo apt-get update
sudo apt-get install -y curl git maven sublime-text-installer atom brackets

curl -sL https://deb.nodesource.com/setup_0.10 | sudo bash -
sudo apt-get install -y nodejs
sudo npm install -g npm

sudo apt-get autoremove
```

```
sudo npm install -g grunt-cli gulp bower yo http-server karma-cli protractor
```


To install a WebStorm menu entry:
cp /usr/share/applications/google-chrome.desktop -\> webstorm.desktop
Edit Name, Exec and Icon






