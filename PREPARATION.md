#JavaScript Workshop ch/open 2015


**Important:** Make sure that you have unrestricted access to the internet! In the steps below and in the workshop you will have to access GitHub and npmjs.org. Some corporate proxies block these sites or access over https/git!

In the workshop the attendees can work through many demos and exercises. The attendees will work on their own laptops. 
To get the environment set up, the attendees have **two possibilities**:

1. Download the prepared VMWare image and run it with either VMWare or VirtualBox.  
The image can be downloaded from http://images.workshoptage.ch/images/ws1/Xubuntu-14.10-chopen.zip  
If you are using this VM, you don't have to prepare the steps below.

2. Prepare the environment on your own laptop with the following steps.

If there are any problems or questions regarding this setup, please contact me at: jonas.bandi@gmail.com




##Software Installation
For the workshop the following software should be installed.  
**The version numbers don't have to match exactly, but should be pretty close!**

### Git
A recent git installation is needed to download the exercise files.  
Check:  
```
> git --version                                                             
git version 2.2.1
```


### Node.js & NPM & Node Packages
Node and NPM is the fundament for the JavaScript Toolchain.
The Node installer can be downloaded here: http://nodejs.org/download/

Check:
```
> node --version
v0.12.2
> npm --version
2.7.5
```

The following npm packages should be installed globally:
```
npm install -g grunt-cli gulp bower yo http-server karma-cli protractor
```

After installing protractor with the above command, you should also run the following command:

	webdriver-manager update


### Browser
A recent Chrome and/or Firefox Browser must be available.  
The following extensions should be installed:

- [Postman Extension for Chrome](https://chrome.google.com/webstore/detail/postman-rest-client-packa/fhbjgbiflinjbdggehcddcbncdddomop?hl=en)
- [RESTClient AddOn for Firefox](https://addons.mozilla.org/en-US/firefox/addon/restclient/)

### WebStorm
The WebStorm IDE from JetBrains should be installed. 
There is a free 30 day trial version of WebStorm available here: http://www.jetbrains.com/webstorm/.

WebStorm is not a requirement for the workshop. However the examples and demos will be shown with WebStorm. It is up to the attendees to use any other editor of their preference.


## Exercise & Demo Preparations

To reduce the network traffic during the course the following steps should be executed before the workshop:

Check out the following git repository:  

	git clone https://bitbucket.org/jonasbandi/js-chopen

From within the cloned repository execute the following commands:

	cd 01-Intro/02-SimpisticToDo-Gulp
	npm install
	...
	bower install
	...

The above step did download all the dependencies to build and run the demo application.  
The build should now work by running the following command:

	gulp

