# Dependency Management

Install all build dependencies and front-end assets of the project:

	npm install
	bower install
	
# Run the Build (linting, asset processing)

	grunt
	
Inspect the result in `dist`


# Run the Unit Tests (Mocha)

	grunt test
	
# Run the Unit Tests (Karma)

	grunt karma

Or run Karma directly:

	karma start
	
Or run Karma from WebStrom.


